package ch.ethz.dalab.javaexample;

import ch.ethz.dalab.dissolve.classification.StructSVMModel;
import ch.ethz.dalab.dissolve.optimization.DissolveFunctions;
import breeze.linalg.Vector;
import scala.Double;

/**
 * Created by tribhu on 30/03/15.
 */
public class BinarySVMFunctions implements DissolveFunctions<Vector<Double>, Double> {
    @Override
    public Vector<Object> featureFn(Vector<Double> doubleVector, Double aDouble) {
        return null;
    }

    @Override
    public Double oracleFn(StructSVMModel<Vector<Double>, Double> model, Vector<Double> doubleVector, Double aDouble) {
        return null;
    }

    @Override
    public double lossFn(Double yPredicted, Double yTruth) {
        return 0;
    }

    @Override
    public Double predictFn(StructSVMModel<Vector<Double>, Double> model, Vector<Double> doubleVector) {
        return null;
    }
}
