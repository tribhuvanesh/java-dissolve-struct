package ch.ethz.dalab.javaexample;

import ch.ethz.dalab.dissolve.classification.BinarySVMWithDBCFW;
import ch.ethz.dalab.dissolve.classification.StructSVMModel;
import ch.ethz.dalab.dissolve.optimization.SolverOptions;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.util.MLUtils;

/**
 * Created by tribhu on 30/03/15.
 */
public class BinaryClassification {
    public static void main(String args[]) {
        SparkConf sparkConf = new SparkConf().setAppName("JavaCOV").setMaster("local");
        JavaSparkContext ctx = new JavaSparkContext(sparkConf);

        SolverOptions solverOptions = new SolverOptions();

        JavaRDD<LabeledPoint> dataRaw = MLUtils.loadLibSVMFile(ctx.sc(), "covtype.libsvm.binary.scale.head").toJavaRDD();

        JavaRDD<LabeledPoint> data = dataRaw.map(
                new Function<LabeledPoint, LabeledPoint>() {
                    public LabeledPoint call(LabeledPoint x) {
                        if (x.label() > 0.5)
                            return new LabeledPoint(1.0, x.features());
                        else
                            return new LabeledPoint(-1.0, x.features());
                    }
                }
        );

        JavaRDD<LabeledPoint>[] splits = data.randomSplit(new double[]{0.8, 0.2}, 1L);
        JavaRDD<LabeledPoint> training = splits[0];
        JavaRDD<LabeledPoint> testing = splits[1];

        StructSVMModel model = BinarySVMWithDBCFW.train(training.rdd(), solverOptions);
    }
}
