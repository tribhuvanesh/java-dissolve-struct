# Structured Prediction with dissolve<sup>struct</sup> in Java #

### Requirements ###
* This repository (`git clone git@bitbucket.org:tribhuvanesh/java-dissolve-struct.git`)
* Maven (`brew install maven` on Mac)
* dissolve^struct in local Maven repository. Can be published like so:
```
#!bash

mvn install:install-file \
    -Dfile=/Users/tribhu/.ivy2/local/ch.ethz.dalab/dissolvestruct_2.10/0.1-SNAPSHOT/jars/dissolvestruct_2.10.jar \
    -DgroupId=ch.ethz.dalab \
    -DartifactId=dissolvestruct_2.10 \
    -Dversion=0.1-SNAPSHOT \
    -Dpackaging=jar \
    -DgeneratePom=true
```


### To run Binary Classification on Forest Cover Dataset ###
1. Change the code to point to the [COV Dataset](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html#covtype.binary).
2. Run `mvn package` within the java-dissolve-struct directory.
3. Execute using:
```
#!bash

./bin/spark-submit \
    --jars ~/.ivy2/local/ch.ethz.dalab/dissolvestruct_2.10/0.1-SNAPSHOT/jars/dissolvestruct_2.10.jar \
    --class "ch.ethz.dalab.javaexample.BinaryClassification" 
    --master local \
    ~/dev-local/java-dissolve-struct/target/java-dissolve-struct-1.0-SNAPSHOT.jar
```